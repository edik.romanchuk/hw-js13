const stopBtn = document.getElementById('btn1');
const startBtn = document.getElementById('btn2');

let offset = 0;
const sliderLine = document.querySelector('.slider-line')

function slide(){
    offset += 200;
    sliderLine.style.left = -offset + 'px'
    if(offset > 400) {
        offset = -200;
    }
}

let timer = setInterval(slide, 3000);

stopBtn.addEventListener('click', () => clearInterval(timer));
startBtn.addEventListener('click', () => timer = setInterval(slide, 3000));

stopBtn.addEventListener('mousedown', () => stopBtn.classList.toggle('active'));
stopBtn.addEventListener('mouseup', () => stopBtn.classList.toggle('active'));

startBtn.addEventListener('mousedown', () => startBtn.classList.toggle('active'));
startBtn.addEventListener('mouseup', () => startBtn.classList.toggle('active'));
